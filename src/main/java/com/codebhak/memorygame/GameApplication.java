package com.codebhak.memorygame;

import android.app.Application;

import com.codebhak.memorygame.utils.FontLoader;

public class GameApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		FontLoader.loadFonts(this);

	}
}
