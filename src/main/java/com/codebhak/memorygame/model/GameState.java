package com.codebhak.memorygame.model;

public class GameState {

	public int remainedSeconds;
	public int achievedStars;
	public int achievedScore;
}
