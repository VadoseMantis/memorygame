package com.codebhak.memorygame.events;

import com.codebhak.memorygame.events.engine.FlipDownCardsEvent;
import com.codebhak.memorygame.events.engine.GameWonEvent;
import com.codebhak.memorygame.events.engine.HidePairCardsEvent;
import com.codebhak.memorygame.events.ui.BackGameEvent;
import com.codebhak.memorygame.events.ui.DifficultySelectedEvent;
import com.codebhak.memorygame.events.ui.FlipCardEvent;
import com.codebhak.memorygame.events.ui.NextGameEvent;
import com.codebhak.memorygame.events.ui.ResetBackgroundEvent;
import com.codebhak.memorygame.events.ui.StartEvent;
import com.codebhak.memorygame.events.ui.ThemeSelectedEvent;


public interface EventObserver {

	void onEvent(FlipCardEvent event);

	void onEvent(DifficultySelectedEvent event);

	void onEvent(HidePairCardsEvent event);

	void onEvent(FlipDownCardsEvent event);

	void onEvent(StartEvent event);

	void onEvent(ThemeSelectedEvent event);

	void onEvent(GameWonEvent event);

	void onEvent(BackGameEvent event);

	void onEvent(NextGameEvent event);

	void onEvent(ResetBackgroundEvent event);

}
